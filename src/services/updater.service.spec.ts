import UpdaterService from './updater.service';

describe('UpdaterService', () => {

	let _classUnderTest: UpdaterService;

	describe('getVersion', () => {

		it('should return 1.0.0', (done) => {
			_classUnderTest = new UpdaterService();

			_classUnderTest.getVersion()
			.then((actual) => {
				expect(actual).toEqual('1.0.0');
				done();
			});
		});

	});
});