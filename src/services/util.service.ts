export default class UtilService {
	public static now(addDays?: number): string {
		let tomorrow: Date = new Date();
		tomorrow.setDate(tomorrow.getDate() + (addDays || 0)); // even 32 is acceptable
		let year: string = tomorrow.getFullYear().toString();
		let mth: string = ('00' + (tomorrow.getMonth() + 1)).slice(-2);
		let day: string = ('00' + tomorrow.getDate()).slice(-2);
		return `${year}-${mth}-${day}`;
	}

	public devMode: boolean = false;
}