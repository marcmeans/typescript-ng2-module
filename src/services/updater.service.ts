import {Injectable} from '@angular/core';

@Injectable()
export default class UpdaterService {

	private _window: Window = <any>(window);

	public getVersion(): Promise<string> {
		return Promise.resolve('1.0.0');
	}

}