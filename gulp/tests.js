/**
 * Gulp test tasks.
 */

'use strict';

var gulp = require('gulp-help')(require('gulp')),
	conf = require('./conf');


/**
 * Gulp test with jasmine and karma configuration.
 */
gulp.task('test', function() {
	var karma = require('karma').Server;

	new karma({
			configFile: __dirname +  '/karma.config.js',
			singleRun: true
		}
	).start();
});
