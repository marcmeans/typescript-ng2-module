/**
 * Gulp common lint tasks.
 */

'use strict';

var gulp = require('gulp-help')(require('gulp')),
    conf = require('./conf'),
    $ = require('gulp-load-plugins')();

/**
 * Gulp tslint task.
 * Run TSLint and report errors.
 * Report errors on pipe process.
 */
gulp.task('tslint', function () {
    return gulp.src(conf.tsFilesGlob)
        .pipe($.tslint())
        .pipe($.tslint.report(conf.reports.tslint_report_type, {
            emitError: false
        }))
        .on('error', conf.errorHandler(conf.errors.title.TYPESCRIPT));
});
