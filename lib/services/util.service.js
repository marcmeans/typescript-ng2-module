"use strict";
var UtilService = (function () {
    function UtilService() {
        this.devMode = false;
    }
    UtilService.now = function (addDays) {
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + (addDays || 0)); // even 32 is acceptable
        var year = tomorrow.getFullYear().toString();
        var mth = ('00' + (tomorrow.getMonth() + 1)).slice(-2);
        var day = ('00' + tomorrow.getDate()).slice(-2);
        return year + "-" + mth + "-" + day;
    };
    return UtilService;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = UtilService;
