# tsngmod
Typescript Angular2 module example

based on [yohangz generator-typescript-npm-bower](https://github.com/yohangz/generator-typescript-npm-bower)



## Table of Contents

- [Installation](#installation)
- [Getting started](#getting-started)
- [How to use](#how-to-use)
- [Publishing your code](#publishing-your-code)
- [Changelog](#changelog)
- [License](#license)

## Installation

```
npm install tsngmod --save
```

## Getting Started

* Run: `npm install` inside your project to install dependencies.
* Run: `npm install typings -g` (If [typings](https://www.npmjs.com/package/typings) is not installed before run this command)
* Run:`npm install gulp -g` to install [Gulp](https://www.npmjs.com/package/gulp) globally
* Run:`npm install rimraf -g` to install globally

## How to Use (FIX)

Here is the list of tasks available out of the box and run these via `npm run <task>`
```
  build             Perform lib build
  clean             Cleans artifacts and regenerate
  test              Run spec tests
  debug             FIX

```

## Publishing Your Code (FIX)

*Once your tests are passing (ideally with a Travis CI green run), you might be ready to publish your code to npm.*

Bumping version number and tagging the repository with it can be done as mentioned below.
For more details read [http://semver.org/](http://semver.org/)
 
Available options to update version 
```  
npm run patch     # makes v0.1.0 → v0.1.1
npm run feature   # makes v0.1.1 → v0.2.0
npm run release   # makes v0.2.1 → v1.0.0
```
Publishing updated version can be done via,
```
npm run <release | feature | patch>
npm publish
```

## Changelog
Recent changes can be viewed on the [CHANGELOG.md](CHANGELOG.md)


